import requests as req
import warnings


# this function performs the integration with the 3rd party APIs. Performs a GET request based
# on the URL, the optional parameters and optional headers provided by the user and returns the response as json object


def get_request_client(url, parameters=None, headers=None):
    # perform get request to retrieve json data
    response = req.get(url, params=parameters, headers=headers)
    # create a dataframe from a json object
    if response.status_code == 200:
        return response.json()
    else:
        warn_msg = str(response.status_code)+response.text
        warnings.warn(warn_msg)
