import operator
from datetime import timedelta
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import PolynomialFeatures
import clientRequest as client
import dbConnection as dbConn
import os
import configparser

# GENERAL API PARAMETERS
lat = -37.984047
lon = -1.128575

# define dates that the model will be trained
start_date = np.datetime64('2020-03-18')
end_date = np.datetime64('2020-03-31')

# Weatherbit_API
short_start_date = '03-18'
short_end_date = '03-31'

# define city code that we investigate
city_code = 'MC'

# Get properties for integration with external API
thisfolder = os.path.dirname(os.path.abspath(__file__))
initfile = os.path.join(thisfolder, 'ConfigFile.properties')
config = configparser.RawConfigParser()
config.read(initfile)
database_username = config.get('DatabaseSection', 'database_username')
database_password = config.get('DatabaseSection', 'database_password')

# PARAMETERS TO INTEGRATE WITH WEATHERBIT API
WEATHERBIT_tp = 'daily'
WEATHERBIT_API_url = config.get('WeatherbitAPI', 'WEATHERBIT_API_url')
WEATHERBIT_key = config.get('WeatherbitAPI', 'WEATHERBIT_key')

# creating array of dates between start and end date
num_of_days = (end_date - start_date).astype(int)
date_range = pd.date_range(start_date, periods=num_of_days).tolist()

# PARAMETERS TO INTEGRATE WITH UV RADIATION API
UV_radiation_API_url = config.get('UV_radiation_API', 'UV_radiation_API_url')
UV_radiation_API_key = config.get('UV_radiation_API', 'UV_radiation_API_key')

# date that we retrieve data for
date = np.datetime64('2020-03-18')
# define all query parameters that we will use
UV_radiation_API_parameters = {'lat': lat, 'lng': lon, 'dt': date}
# define access token in header
UV_radiation_API_headers = {'x-access-token': UV_radiation_API_key}

# RETRIEVE DATASET FROM WEATHERBIT API
# https://www.weatherbit.io/api/weather-energy
# the api does not support date range requests so we perform subsequent daily requests for the range startdate-enddate
# for WEATHERBIT_start_date in date_range:
# loop through the date list
# WEATHERBIT_end_date = WEATHERBIT_start_date + timedelta(days=1)
WEATHERBIT_API_parameters = {'lat': lat, 'lon': lon, 'start_day': short_start_date,
                             'end_day': short_end_date, 'tp': WEATHERBIT_tp,
                             'key': WEATHERBIT_key}
json_WEATHERBIT_dataset = client.get_request_client(WEATHERBIT_API_url, WEATHERBIT_API_parameters)
climate_data = pd.DataFrame.from_dict(json_WEATHERBIT_dataset['data'], orient='columns')

# RETRIEVE DATASET FROM OPENUV API
# https://www.openuv.io/uvindex
json_UV_radiation_dataset = client.get_request_client(UV_radiation_API_url, UV_radiation_API_parameters,
                                                      UV_radiation_API_headers)
pd_UV_radiation_dataset = pd.DataFrame.from_dict(json_UV_radiation_dataset, orient='columns')

# Processing training climate dataset
climate_data = climate_data[['dewpt', 'wind_spd', 'temp']]
# Processing R dataset
# fetch data from database
R_data = dbConn.get_rvalues_by_city_code("evaluated_r", city_code)

# converting to date time to mask dates
R_data['Date'] = pd.to_datetime(R_data['Date']).dt.strftime('%d/%m/%Y')
R_data['Date'] = pd.to_datetime(R_data['Date'])
mask2 = ((R_data['Date'] >= start_date) & (R_data['Date'] <= end_date) & (R_data['City'] == city_code))
R_data = R_data.loc[mask2]
date_window = R_data['Date']

# Inserting column of R to the climate dataset
# To copy a column of a dataframe to another dataframe you have to set the two indexes equal
R_data = R_data.set_index(climate_data.index)
# Parse Most Likely column as float
R_data['ML'] = R_data['ML'].astype(float)
R_values = R_data['ML']
climate_data['R_value_Bayessian'] = R_values

# Independent variables
x = climate_data[['dewpt', 'wind_spd', 'temp']]
# Dependent variable R
y = climate_data['R_value_Bayessian']

# transforming the data to include another axis
y = y[:, np.newaxis]

polynomial_features = PolynomialFeatures(degree=4)
x_poly = polynomial_features.fit_transform(x)

model = LinearRegression()
model.fit(x_poly, y)
y_poly_pred = model.predict(x_poly)

# Convert 2D array to 1D
y_polu_1D = y_poly_pred.flatten()
# Create series from array
R_values_series = pd.Series(y_polu_1D)
# Create data array for the dataframe construction
data = list(zip(date_window, R_values_series))
predicted_values = pd.DataFrame(data=data, columns=['Date', 'R_value'])
# add city code column
predicted_values['City'] = city_code

# DATAFRAME COLUMNS
# Date, R_value, City

# Insert dataframe to mysql database table
dbConn.insert_dataframe_to_db(predicted_values, 'r_data')

rmse = np.sqrt(mean_squared_error(y, y_poly_pred))
r2 = r2_score(y, y_poly_pred)
print(rmse)
print(r2)

# sort the values of x before line plot
sort_axis = operator.itemgetter(0)
sorted_zip = sorted(zip(x, y_poly_pred), key=sort_axis)
x, y_poly_pred = zip(*sorted_zip)
